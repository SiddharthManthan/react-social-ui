import axios from "axios";

export const makeRequest = axios.create({
    //TODO: Hardcoded url
    baseURL: `${import.meta.env.VITE_API_URL}/api/`,
    withCredentials: true,
});
