import "./rightBar.scss";

const RightBar = () => {
    return (
        <div className="rightBar">
            <div className="container">
                <div className="item">
                    <span>Suggestions For You</span>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-spencer-selover-428328.jpg"
                                alt=""
                            />
                            <span>Jack Eden</span>
                        </div>
                        <div className="buttons">
                            <button>Follow</button>
                            <button>Dismiss</button>
                        </div>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-tuấn-kiệt-jr-1308881.jpg"
                                alt=""
                            />
                            <span>Natalie Wells</span>
                        </div>
                        <div className="buttons">
                            <button>Follow</button>
                            <button>Dismiss</button>
                        </div>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-tuấn-kiệt-jr-1382726.jpg"
                                alt=""
                            />
                            <span>Milly Hills</span>
                        </div>
                        <div className="buttons">
                            <button>Follow</button>
                            <button>Dismiss</button>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <span>Latest Activities</span>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-tuấn-kiệt-jr-1308881.jpg"
                                alt=""
                            />
                            <p>
                                <span>Natalie Wells</span> created new story
                                picture
                            </p>
                        </div>
                        <span>1 min ago</span>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-spencer-selover-428328.jpg"
                                alt=""
                            />
                            <p>
                                <span>Jack Eden</span> changed their cover
                                picture
                            </p>
                        </div>
                        <span>1 min ago</span>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-stanislav-kondratiev-2909077.jpg"
                                alt=""
                            />
                            <p>
                                <span>John Doe</span> created new post
                                picture
                            </p>
                        </div>
                        <span>1 min ago</span>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-tuấn-kiệt-jr-1382726.jpg"
                                alt=""
                            />
                            <p>
                                <span>Milly Hills</span> created new story
                                picture
                            </p>
                        </div>
                        <span>1 min ago</span>
                    </div>
                </div>
                <div className="item">
                    <span>Online Friends</span>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-spencer-selover-428328.jpg"
                                alt=""
                            />
                            <div className="online" />
                            <span>Jack Eden</span>
                        </div>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-tuấn-kiệt-jr-1308881.jpg"
                                alt=""
                            />
                            <div className="online" />
                            <span>Natalie Wells</span>
                        </div>
                    </div>
                    <div className="user">
                        <div className="userInfo">
                            <img
                                src="/upload/pexels-tuấn-kiệt-jr-1382726.jpg"
                                alt=""
                            />
                            <div className="online" />
                            <span>Milly Hills</span>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    );
};

export default RightBar;
