import "./stories.scss";
import { useContext } from "react";
import { AuthContext } from "../../context/authContext";

import React from "react";

const Stories = () => {
    const { currentUser } = useContext(AuthContext);

    // Temporary Data
    const stories = [
        {
            id: 1,
            name: "Jane Doe",
            img: "/upload/pexels-alex-azabache-3250362.jpg",
        },
        {
            id: 2,
            name: "Natalie Wells",
            img: "/upload/pexels-gantas-vaičiulėnas-1891882.jpg",
        },
        {
            id: 3,
            name: "Jack Eden",
            img: "/upload/pexels-angelo-duranti-3512846.jpg",
        },
        {
            id: 4,
            name: "Milly Hills",
            img: "/upload/pexels-elianne-dipp-4666754.jpg",
        },
    ];

    return (
        <div className="stories">
            <div className="story">
                <img src={"/upload/" + currentUser.profilePic} alt="" />
                <span>{currentUser.name}</span>
                <button>+</button>
            </div>
            {stories.map((story) => (
                <div className="story" key={story.id}>
                    <img src={story.img} alt="" />
                    <span>{story.name}</span>
                </div>
            ))}
        </div>
    );
};

export default Stories;
